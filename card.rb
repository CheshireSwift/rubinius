class Card
  class << self; attr_accessor :cards end
  @cards = {}

  attr_accessor :name
  attr_accessor :type
  attr_accessor :cost
  attr_accessor :actions
  attr_accessor :buys
  attr_accessor :coins
  attr_accessor :cards
  attr_accessor :effect

  def initialize(name, type, cost, actions: 0, buys: 0, coins: 0, cards: 0, effect: nil)
    @name = name
    @type = type
    @cost = cost
    @actions = actions
    @buys = buys
    @coins = coins
    @cards = cards
    @effect = effect
  end
end
