require 'sinatra'
require 'sinatra/reloader' if development?
Dir[File.dirname(__FILE__) + '/cards/*.rb'].each {|file| require file}
require 'haml'

get '/card/:card' do |card_name|
  @types = {:action => 'Action'}
  haml :renderer, :locals => {:card => Card.cards[card_name]}
end
